package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.SingleUser
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection

object GetSingleUser : Callback<SingleUser> {
    override fun onResponse(call: Call<SingleUser>?, response: Response<SingleUser>?) {
        if ( response?.code() == HttpURLConnection.HTTP_NOT_FOUND )
        {
            Log.d( RetrofitReqres.TAG_TO_LOG, "There is no user with that number" )
        } else {
            Log.d( RetrofitReqres.TAG_TO_LOG, "name: ${response?.body()?.data?.first_name}" +
                    ", surname: ${response?.body()?.data?.last_name}" )
        }
    }

    override fun onFailure(call: Call<SingleUser>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}