package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.UsersList
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object GetUsers : Callback<UsersList> {
    override fun onResponse(call: Call<UsersList>?, response: Response<UsersList>?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "got users from page ${response?.body()?.page}" )
        Log.d( RetrofitReqres.TAG_TO_LOG, "found ${response?.body()?.data?.size}" )
    }

    override fun onFailure(call: Call<UsersList>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}