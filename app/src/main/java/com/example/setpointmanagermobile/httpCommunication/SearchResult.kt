package com.example.setpointmanagermobile.httpCommunication

class SearchResult {
    var start = 0
    var num_found = 0
    var docs = mutableListOf<Document>()
}