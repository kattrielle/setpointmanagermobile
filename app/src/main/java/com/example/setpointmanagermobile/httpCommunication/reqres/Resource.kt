package com.example.setpointmanagermobile.httpCommunication.reqres

class Resource {
    var id = 0
    var name = ""
    var year = 0
    var color = ""
    var pantone_value = ""
}