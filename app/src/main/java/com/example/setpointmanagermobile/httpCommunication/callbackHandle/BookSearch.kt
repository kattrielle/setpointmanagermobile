package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.setpointmanagermobile.httpCommunication.SearchResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object BookSearch : Callback<SearchResult> {
    override fun onResponse(call: Call<SearchResult>?, response: Response<SearchResult>? ) {
        Log.d( "books", "entered response, finded ${response?.body()?.num_found} books" )

        //todo переделать toast в вызов пиналки этого тоста
        //Toast.makeText( context, "Найдено книг: " + response?.body()?.num_found,
        //    Toast.LENGTH_LONG ).show()
    }

    override fun onFailure(call: Call<SearchResult>?, t: Throwable? ) {
        t?.message?.let { Log.d( "books", it ) }
    }
}