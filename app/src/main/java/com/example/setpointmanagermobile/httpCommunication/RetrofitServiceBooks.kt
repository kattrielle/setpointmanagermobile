package com.example.setpointmanagermobile.httpCommunication

import retrofit2.Call
import retrofit2.http.*

interface RetrofitServiceBooks {
    /*
        http://openlibrary.org/search.json?author=tolkien
    */
    @GET("search.json")
    fun findBooksByAuthor( @Query( "author" ) author: String ) : Call<SearchResult>

}