package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.SingleResource
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection

object GetSingleResource : Callback<SingleResource> {
    override fun onResponse(call: Call<SingleResource>?, response: Response<SingleResource>?) {
        if ( response?.code() == HttpURLConnection.HTTP_NOT_FOUND )
        {
            Log.d( RetrofitReqres.TAG_TO_LOG, "There is no resource with that number" )
        } else {
            Log.d( RetrofitReqres.TAG_TO_LOG, "name: ${response?.body()?.data?.name}" )
        }
    }

    override fun onFailure(call: Call<SingleResource>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}