package com.example.setpointmanagermobile.httpCommunication.retrofitBuilders

import com.example.setpointmanagermobile.httpCommunication.RetrofitServiceReqres
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitReqres {
    val TAG_TO_LOG = "reqres_task"

    val client = OkHttpClient.Builder()
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl( "https://reqres.in" )
        .addConverterFactory( GsonConverterFactory.create( Gson() ) )
        .client( client )
        .build()

    val service = retrofit.create( RetrofitServiceReqres::class.java )
}