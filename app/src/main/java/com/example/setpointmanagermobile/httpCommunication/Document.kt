package com.example.setpointmanagermobile.httpCommunication

class Document {
    var cover_i = 0
    var has_fulltext = false
    var edition_count = 0
    var title = ""
    var author_name = mutableListOf<String>()
    var first_publish_year = 0
    var key = ""
    var ia = mutableListOf<String>()
    var author_key = mutableListOf<String>()
    var public_scan_b = false
}