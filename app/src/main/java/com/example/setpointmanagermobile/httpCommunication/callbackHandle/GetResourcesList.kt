package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.ResourcesList
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object GetResourcesList : Callback<ResourcesList> {
    override fun onResponse(call: Call<ResourcesList>?, response: Response<ResourcesList>?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "got resources from page ${response?.body()?.page}" )
        Log.d( RetrofitReqres.TAG_TO_LOG, "found ${response?.body()?.data?.size}" )
    }

    override fun onFailure(call: Call<ResourcesList>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}