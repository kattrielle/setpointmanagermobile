package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.AuthAnswer
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection.*

object RegisterUser : Callback<AuthAnswer> {
    override fun onResponse(call: Call<AuthAnswer>?, response: Response<AuthAnswer>?) {
        when( response?.code() )
        {
            HTTP_OK -> Log.d( RetrofitReqres.TAG_TO_LOG, "successfully created!" )
            HTTP_BAD_REQUEST -> Log.d( RetrofitReqres.TAG_TO_LOG,
                "Registration failed. error: ${response.errorBody()?.source()}" )
        }
    }

    override fun onFailure(call: Call<AuthAnswer>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}