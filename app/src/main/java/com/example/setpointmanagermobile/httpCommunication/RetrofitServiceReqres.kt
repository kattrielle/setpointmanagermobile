package com.example.setpointmanagermobile.httpCommunication

import com.example.setpointmanagermobile.httpCommunication.reqres.*
import retrofit2.Call
import retrofit2.http.*

interface RetrofitServiceReqres {

    /*
        https://reqres.in/api/users?page=2
     */
    @GET( "api/users" )
    fun getUsersList( @Query( "page" ) page: Int) : Call<UsersList>

    /*
        https://reqres.in/api/users/2
     */
    @GET( "api/users/{user}" )
    fun getUserInfo( @Path( "user" ) userNum: Int) : Call<SingleUser>

    /*
        https://reqres.in/api/unknown?page=1
     */
    @GET( "api/unknown" )
    fun getResourcesList( @Query( "page" ) page: Int) : Call<ResourcesList>

    /*
        https://reqres.in/api/unknown/5
     */
    @GET( "api/unknown/{id}")
    fun getResourceInfo( @Path( "id" ) id: Int) : Call<SingleResource>


    /*

     */
    @POST( "api/users" )
    fun createUser( @Body user: UserCreate) : Call<UserJobData>

    /*

     */
    @PUT( "api/users/{id}" )
    fun updateUser(@Path( "id" ) id: Int, @Body user: UserCreate) : Call<UserJobData>

    /*

     */
    @PATCH( "api/users/{id}" )
    fun patchUser(@Path( "id" ) id: Int, @Body user: UserCreate) : Call<UserJobData>

    /*

     */
    @DELETE( "api/users/{id}" )
    fun deleteUser( @Path( "id" ) id: Int ) : Call<UserJobData>

    /*

     */
    @POST( "api/register" )
    fun registerUser( @Body user: AuthInfo ) : Call<AuthAnswer>

    /*

     */
    @POST( "api/login" )
    fun login( @Body user: AuthInfo ) : Call<AuthAnswer>

    /*

     */
    @GET( "api/users" )
    fun getUsersListWithDelay( @Query( "delay" ) delay : Int ) : Call<UsersList>
}