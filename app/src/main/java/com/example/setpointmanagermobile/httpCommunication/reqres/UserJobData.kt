package com.example.setpointmanagermobile.httpCommunication.reqres

class UserJobData {
    var name: String? = ""
    var job: String? = ""
    var id: String? = ""
    var createdAt: String? = ""
    var updatedAt: String? = ""
}