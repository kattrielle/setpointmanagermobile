package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.UserJobData
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection

object DeleteUser : Callback<UserJobData> {
    override fun onResponse(call: Call<UserJobData>?, response: Response<UserJobData>?) {
        if ( response?.code() == HttpURLConnection.HTTP_NO_CONTENT )
        {
            Log.d( RetrofitReqres.TAG_TO_LOG, "successfully deleted!" )
        } else {
            Log.d( RetrofitReqres.TAG_TO_LOG, "unexpected answer at deleting" )
        }
    }

    override fun onFailure(call: Call<UserJobData>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}