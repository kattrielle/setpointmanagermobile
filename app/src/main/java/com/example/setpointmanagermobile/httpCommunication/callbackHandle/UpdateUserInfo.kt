package com.example.setpointmanagermobile.httpCommunication.callbackHandle

import android.util.Log
import com.example.setpointmanagermobile.httpCommunication.reqres.UserJobData
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object UpdateUserInfo : Callback<UserJobData> {
    override fun onResponse(call: Call<UserJobData>?, response: Response<UserJobData>?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "User with name ${response?.body()?.name} updated at ${response?.body()?.updatedAt}" )
    }

    override fun onFailure(call: Call<UserJobData>?, t: Throwable?) {
        Log.d( RetrofitReqres.TAG_TO_LOG, "something went wrong:" + t?.message )
    }

}