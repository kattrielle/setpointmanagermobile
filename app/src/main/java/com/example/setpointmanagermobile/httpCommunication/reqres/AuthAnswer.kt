package com.example.setpointmanagermobile.httpCommunication.reqres

class AuthAnswer {
    var id: Int? = 0
    var token: String? = ""
    var error: String? = ""
}