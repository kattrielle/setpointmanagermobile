package com.example.setpointmanagermobile.httpCommunication.reqres

class UsersList {
    var page = 0
    var per_page = 0
    var total = 0
    var total_pages = 0
    val data = mutableListOf<UserData>()
    var support = Support()
}