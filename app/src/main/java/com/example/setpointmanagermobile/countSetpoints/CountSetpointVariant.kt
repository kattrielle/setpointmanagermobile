package com.example.setpointmanagermobile.countSetpoints

enum class CountSetpointVariant {
    Maximum,
    Average,
    MaximumMSD, //среднеквадратичное отклонение от максимума
    AverageMSD
}