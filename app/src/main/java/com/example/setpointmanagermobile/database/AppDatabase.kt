package com.example.setpointmanagermobile.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database( entities = arrayOf(SetpointsBaseProxy::class), version = 1 )
abstract class AppDatabase : RoomDatabase() {
    abstract fun setpointsDao(): SetpointsDao
}