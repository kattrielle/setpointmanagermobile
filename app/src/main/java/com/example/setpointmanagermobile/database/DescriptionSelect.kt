package com.example.setpointmanagermobile.database

import androidx.room.ColumnInfo

data class DescriptionSelect(
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "description") val description: String
)
