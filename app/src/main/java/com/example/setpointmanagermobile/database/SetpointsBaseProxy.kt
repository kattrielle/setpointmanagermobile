package com.example.setpointmanagermobile.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class SetpointsBaseProxy (
    @PrimaryKey val id: Int,
    val setpointMap: String,
    val description: String
)