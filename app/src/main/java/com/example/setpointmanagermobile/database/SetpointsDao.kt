package com.example.setpointmanagermobile.database

import androidx.room.*

@Dao
interface SetpointsDao {
    @Insert( onConflict = OnConflictStrategy.IGNORE )
    fun insertAll( vararg item: SetpointsBaseProxy )

    @Query( "SELECT id,description from setpointsBaseProxy" )
    fun getDescriptions() : List<DescriptionSelect>

    @Query( "SELECT setpointMap from setpointsBaseProxy where id LIKE :id" )
    fun getSelectedCollection( id: Int ) : String

    @Delete
    fun delete( item: SetpointsBaseProxy )
}