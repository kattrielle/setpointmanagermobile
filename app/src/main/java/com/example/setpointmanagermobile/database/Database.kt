package com.example.setpointmanagermobile.database

import androidx.room.Room
import com.example.setpointmanagermobile.App

object Database {
    val LOG_TEXT = "room_log"

    val db = Room.databaseBuilder(
        App.getContext(),
        AppDatabase::class.java, "setpoints"
    ).allowMainThreadQueries()
        .build()

    val setpointsDao = db.setpointsDao()

}