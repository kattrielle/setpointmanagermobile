package com.example.setpointmanagermobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.setpointmanagermobile.httpCommunication.callbackHandle.*
import com.example.setpointmanagermobile.httpCommunication.reqres.AuthInfo
import com.example.setpointmanagermobile.httpCommunication.reqres.UserCreate
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitReqres

class ReqresActivity : AppCompatActivity() {
    private lateinit var textUsersPageGet: EditText
    private lateinit var buttonReadUsers: Button
    private lateinit var textUserNumGet: EditText
    private lateinit var buttonReadUser: Button
    private lateinit var textResourcesPageGet: EditText
    private lateinit var buttonReadResources: Button
    private lateinit var textResourceNumGet: EditText
    private lateinit var buttonReadResource: Button
    private lateinit var textUserNameSet: EditText
    private lateinit var textUserJobSet: EditText
    private lateinit var textUserNumSet: EditText
    private lateinit var buttonCreateUser: Button
    private lateinit var buttonUpdateUser: Button
    private lateinit var buttonFixUser: Button
    private lateinit var textUserDelete: EditText
    private lateinit var buttonDeleteUser: Button
    private lateinit var textEmail: EditText
    private lateinit var textPassword: EditText
    private lateinit var buttonRegister: Button
    private lateinit var buttonLogin: Button

    override fun onCreate( savedInstanceState: Bundle? ) {
        super.onCreate( savedInstanceState )
        setContentView( R.layout.activity_reqres )

        textUsersPageGet = findViewById( R.id.edittext_users_page )
        buttonReadUsers = findViewById( R.id.btn_load_some_users )
        textUserNumGet = findViewById( R.id.edittext_single_user )
        buttonReadUser = findViewById( R.id.btn_load_single_user )
        textResourcesPageGet = findViewById( R.id.edittext_resources_list )
        buttonReadResources = findViewById( R.id.btn_load_resources_list )
        textResourceNumGet = findViewById( R.id.edittext_single_resource )
        buttonReadResource = findViewById( R.id.btn_load_single_resource )
        textUserNameSet = findViewById( R.id.edittext_user_name )
        textUserJobSet = findViewById( R.id.edittext_user_job )
        textUserNumSet = findViewById( R.id.edittext_user_num )
        buttonCreateUser = findViewById( R.id.btn_create_user )
        buttonUpdateUser = findViewById( R.id.btn_update_user )
        buttonFixUser = findViewById( R.id.btn_patch_user )
        textUserDelete = findViewById( R.id.edittext_user_delete )
        buttonDeleteUser = findViewById( R.id.btn_user_delete )
        textEmail = findViewById( R.id.edittext_email )
        textPassword = findViewById( R.id.edittext_password )
        buttonRegister = findViewById( R.id.btn_register )
        buttonLogin = findViewById( R.id.btn_login )

        initHandlers()
    }

    private fun initHandlers()
    {
        buttonReadUsers.setOnClickListener { view ->
            val page = textUsersPageGet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.getUsersList( page ).enqueue( GetUsers )
        }
        buttonReadUser.setOnClickListener { view ->
            val num = textUserNumGet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.getUserInfo( num ).enqueue( GetSingleUser )
        }
        buttonReadResources.setOnClickListener { view ->
            val page = textResourcesPageGet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.getResourcesList( page ).enqueue( GetResourcesList )
        }
        buttonReadResource.setOnClickListener { view ->
            val num = textResourceNumGet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.getResourceInfo( num ).enqueue( GetSingleResource )
        }
        buttonCreateUser.setOnClickListener { view ->
            RetrofitReqres.service.createUser( userDataToSend() ).enqueue( CreateUser )
        }
        buttonUpdateUser.setOnClickListener { view ->
            val num =  textUserNumSet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.updateUser( num, userDataToSend() ).enqueue( UpdateUserInfo )
        }
        buttonFixUser.setOnClickListener { view ->
            val num =  textUserNumSet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.patchUser( num, userDataToSend() ).enqueue( UpdateUserInfo )
        }
        buttonDeleteUser.setOnClickListener { view ->
            val num =  textUserNumSet.text.toString().toIntOrNull() ?: 0
            RetrofitReqres.service.deleteUser( num ).enqueue( DeleteUser )
        }
        buttonRegister.setOnClickListener { view ->
            RetrofitReqres.service.registerUser( loginInfo() ).enqueue( RegisterUser )
        }
        buttonLogin.setOnClickListener { view ->
            RetrofitReqres.service.login( loginInfo() ).enqueue( LoginCheck )
        }
    }

    private fun userDataToSend() : UserCreate
    {
        val user = UserCreate()
        user.name = textUserNameSet.text.toString()
        user.job = textUserJobSet.text.toString()
        return user
    }

    private fun loginInfo() : AuthInfo
    {
        val info = AuthInfo()
        info.email = textEmail.text.toString()
        info.password = textPassword.text.toString()
        return info
    }
}