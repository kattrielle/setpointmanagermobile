package com.example.setpointmanagermobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.setpointmanagermobile.database.Database
import com.example.setpointmanagermobile.database.SetpointsBaseProxy
import com.example.setpointmanagermobile.httpCommunication.callbackHandle.BookSearch
import com.example.setpointmanagermobile.httpCommunication.retrofitBuilders.RetrofitOpenlibrary

class MainActivity : AppCompatActivity() {
    private lateinit var buttonReadSetpoints : Button
    private lateinit var buttonReadSample : Button
    private lateinit var buttonCountSetpoint : Button
    private lateinit var buttonShowSetpoints : Button
    private lateinit var buttonWriteSetpoints : Button
    private lateinit var buttonTest : Button

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonReadSetpoints = findViewById( R.id.btn_read_setpoints )
        buttonReadSample = findViewById( R.id.btn_read_samples )
        buttonCountSetpoint = findViewById( R.id.btn_count_setpoints )
        buttonShowSetpoints = findViewById( R.id.btn_view_setpoints_list )
        buttonWriteSetpoints = findViewById( R.id.btn_write_setpoints )
        buttonTest = findViewById( R.id.btn_test )

        setControlHandlers()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        val inflater: MenuInflater = menuInflater
        inflater.inflate( R.menu.main_menu, menu )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when ( item.itemId )
        {
            R.id.menu_synchronize -> {
                Database.setpointsDao.insertAll( SetpointsBaseProxy(1, "",
                    "Набор уставок для датчика 1"), SetpointsBaseProxy(
                    2, "", "Набор уставок для датчика 2" ),
                    SetpointsBaseProxy( 3, "", "Набор уставок для крейта" )
                )
                true
            }
            R.id.menu_select_map -> {
                val results = Database.setpointsDao.getDescriptions()
                results.forEach {
                    Log.d( Database.LOG_TEXT, it.description )
                }
                true
            }
            R.id.menu_settings -> {
                val intent = Intent( this, SettingsActivity::class.java )
                startActivity( intent )
                true
            }
            else -> {
                false
            }
        }
    }

    private fun setControlHandlers()
    {
        buttonReadSetpoints.setOnClickListener { view ->
            onClick_btnReadSetpoints( view ) }
        buttonReadSample.setOnClickListener { view ->
            onClick_btnReadSamples( view ) }
        buttonCountSetpoint.setOnClickListener { view ->
            onClick_btnCountSetpoints( view ) }
        buttonShowSetpoints.setOnClickListener { view ->
            onClick_btnViewSetpoints( view ) }
        buttonWriteSetpoints.setOnClickListener { view ->
            onClick_btnWriteSetpoints( view ) }
        buttonTest.setOnClickListener { view ->
            val intent = Intent( this, ReqresActivity::class.java )
            startActivity( intent )
        }
    }

    private fun initRetrofit()
    {
        RetrofitOpenlibrary.service.findBooksByAuthor( "tolkien" ).enqueue( BookSearch )
    }

    private fun onClick_btnReadSetpoints( view: View )
    {
        //заглушка чтения
        Toast.makeText( this, "Идёт чтение параметров",
            Toast.LENGTH_LONG ).show()
    }

    private fun onClick_btnReadSamples( view: View )
    {
        //заглушка чтения
        initRetrofit()
    }

    private fun onClick_btnCountSetpoints( view: View )
    {

    }

    private fun onClick_btnViewSetpoints( view: View )
    {
        val intent = Intent( this, ShowSetpointsActivity::class.java )
        startActivity( intent )
    }

    private fun onClick_btnWriteSetpoints( view: View )
    {

    }
}