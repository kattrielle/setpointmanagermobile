package com.example.setpointmanagermobile.model.externalDevices.ports

import com.example.setpointmanagermobile.model.externalDevices.settings.SettingsCOM

class PortCOM (name: String) : IPort {

    constructor(settings : SettingsCOM) : this( settings.name )
    {
        SetPortParameters( settings )
    }
    override var isOpen : Boolean = false // @todo fix??? или тут всё уже хорошо?
    get() = true

    override var delayAnswerRead: Int = 100

    override var delayAnswerWrite: Int = 100

    fun SetPortParameters( settings: SettingsCOM) : Boolean
    {
        return true
    }

    override fun OpenPort() : Boolean
    {
        return true
    }

    override fun ClosePort() : Boolean
    {
        return true
    }

    override fun SendMessage(message: String) : Boolean
    {
        return true
    }

    override fun SendMessage(message: Array<Byte>) : Boolean
    {
        return true
    }

    override fun SendQuery(message:String): Pair<Boolean, String>
    {
        return Pair(false, "")
    }

    override fun SendQuery(message: Array<Byte>, answerLen: Int,
                           attempts: Int): Pair<Boolean,Array<Byte>>
    {
        return Pair(false, emptyArray() )
    }
}