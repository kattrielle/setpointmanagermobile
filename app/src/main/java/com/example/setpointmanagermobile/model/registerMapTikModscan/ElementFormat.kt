package com.example.setpointmanagermobile.model.registerMapTikModscan

enum class ElementFormat {
    Int,
    Float,
    swFloat,
    Double,
    swDouble,
    String,
    Int32,
    swInt32
}