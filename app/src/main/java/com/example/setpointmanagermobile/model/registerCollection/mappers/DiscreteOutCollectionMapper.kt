package com.example.setpointmanagermobile.model.registerCollection.mappers

import com.example.setpointmanagermobile.model.registerCollection.DiscreteOutCollection

class DiscreteOutCollectionMapper() {
    var defenceCollection = WriteDefenceCollectionMapper()

    val items = mutableListOf<DiscreteOutMapper>()

    constructor( baseCollection : DiscreteOutCollection) : this()
    {
        defenceCollection = WriteDefenceCollectionMapper( baseCollection.defence )

        for( out in baseCollection.items )
        {
            items.add( DiscreteOutMapper( out ) )
        }
    }
}