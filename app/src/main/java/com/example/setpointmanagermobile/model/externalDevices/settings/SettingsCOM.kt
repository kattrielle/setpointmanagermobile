package com.example.setpointmanagermobile.model.externalDevices.settings

open class SettingsCOM( var name : String ) : PortSettings()
{
	var baudRate : Int = 9600
	var dataBits : Int = 8
    var parity : Int = 0
    var stopBits : Int = 1
    var dtrEnabled : Boolean = false
    var readTimeout : Int = 750
    var writeTimeout : Int = 750

    var delayAnswerRead: Int = 100
    var delayAnswerWrite: Int = 100

	override val description : String
    get() = name

	override fun CheckName() : Boolean
    {
        return name.isNotEmpty()
    }
}