package com.example.setpointmanagermobile.model.registerCollection.mappers

import com.example.setpointmanagermobile.model.registerCollection.DiscreteOutRegister
import com.example.setpointmanagermobile.model.registerMapTikModscan.CellData

class RegisterMapper() {
    var cell : CellData? = null
    
    var value = 0.0

    constructor( baseRegister : DiscreteOutRegister) : this()
    {
        cell = baseRegister.modscanCell
        value = baseRegister.value
    }
}