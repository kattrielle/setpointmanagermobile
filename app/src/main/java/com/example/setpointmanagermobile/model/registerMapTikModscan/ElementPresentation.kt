package com.example.setpointmanagermobile.model.registerMapTikModscan

enum class ElementPresentation {
    Dec,
    Signed,
    Bin,
    Hex
}