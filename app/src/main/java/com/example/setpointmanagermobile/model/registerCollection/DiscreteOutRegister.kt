package com.example.setpointmanagermobile.model.registerCollection

import com.example.setpointmanagermobile.model.externalDevices.devices.Modbus
import com.example.setpointmanagermobile.model.registerCollection.mappers.RegisterMapper
import com.example.setpointmanagermobile.model.registerMapTikModscan.CellData
import com.example.setpointmanagermobile.model.registerMapTikModscan.CellFormulaData
import com.example.setpointmanagermobile.model.registerMapTikModscan.ElementFormat
import com.example.setpointmanagermobile.model.registerMapTikModscan.ElementType

class DiscreteOutRegister() {
    var modscanCell : CellData? = CellData()

    var description : String
        get() = modscanCell?.name ?: ""
    set(value) { modscanCell?.name = value }

    var registerNumber : Int
        get() = modscanCell?.address?.plus(1) ?: 0
        set(value) { modscanCell?.address = value - 1 }

    var value = 0.0

    val isUsed : Boolean
        get() = modscanCell != null

    constructor( mapper : RegisterMapper) : this()
    {
        modscanCell = mapper.cell
        value = mapper.value
    }

    fun writeValue( value : Double, device : Modbus) : Boolean
    {
        return writeHoldingValue( toClearData( value, modscanCell?.formulaData ), device )
    }

    fun readValue( device : Modbus) : Boolean
    {
        val ( success, result ) = getValue(  device )
        value = toValuePresentation( result, modscanCell?.formulaData )
        return success
    }

    fun readConvertedValue( device : Modbus) : Pair< Boolean, Double >
    {
        val ( success, result ) = getValue( device )
        return Pair( success, toValuePresentation( result, modscanCell?.formulaData ) )
    }

    private fun toClearData( value : Double, coeffs : CellFormulaData? ) : Double
    {
        return if ( coeffs == null )
        {
            value
        } else {
            (value - coeffs.sCoeff) / coeffs.kCoeff
        }
    }

    private fun toValuePresentation( value : Double, coeffs : CellFormulaData? ) : Double
    {
        return if ( coeffs == null )
        {
            value
        } else {
            value * coeffs.kCoeff + coeffs.sCoeff
        }
    }

    private fun writeHoldingValue(
        value: Double,
        device: Modbus,
    ) : Boolean
    {
        return when ( modscanCell?.format ) {
            ElementFormat.Int -> {
                device.SetHoldingValue( registerNumber, value.toInt() )
            }
            ElementFormat.Float -> {
                device.SetHoldingSwFloat( registerNumber, value.toFloat() )
            }
            ElementFormat.swFloat -> {
                device.SetHoldingFloat( registerNumber, value.toFloat() )
            }
            else -> false
        }
    }

    private fun getValue( device : Modbus)
            : Pair<Boolean, Double>
    {
        return when ( modscanCell?.type ) {
            ElementType.InputRegister ->
                getInputValue( device )
            ElementType.HoldingRegister ->
                getHoldingValue( device )
            else -> Pair( false, Double.NEGATIVE_INFINITY )
        }
    }

    private fun getHoldingValue(  device : Modbus)
            : Pair<Boolean, Double>
    {
        return when ( modscanCell?.format ) {
            ElementFormat.Int -> {
                val ( success, value ) = device.GetOneHoldingValue( registerNumber )
                Pair( success, value.toDouble() )
            }
            ElementFormat.Float -> {
                val ( success, value ) = device.GetHoldingSwFloat( registerNumber )
                Pair( success, value.toDouble() )
            }
            ElementFormat.swFloat -> {
                val ( success, value ) = device.GetHoldingFloat( registerNumber )
                Pair( success, value.toDouble() )
            }
            else ->
                Pair( false, 0.0 )
        }
    }

    private fun getInputValue( device : Modbus)
            : Pair<Boolean, Double>
    {
        return when ( modscanCell?.format ) {
            ElementFormat.Int -> {
                val (success, value) = device.GetOneInputValue( registerNumber )
                Pair( success, value.toDouble() )
            }
            ElementFormat.Float -> {
                val (success, value) = device.GetInputSwFloat( registerNumber )
                Pair( success, value.toDouble() )
            }
            ElementFormat.swFloat -> {
                val (success, value ) = device.GetInputFloat( registerNumber )
                Pair( success, value.toDouble() )
            }
            else ->
                Pair( false, 0.0 )
        }
    }
}