package com.example.setpointmanagermobile.model.registerCollection

import com.example.setpointmanagermobile.countSetpoints.CountSetpoints
import com.example.setpointmanagermobile.countSetpoints.CountSetpointsDescriptions
import com.example.setpointmanagermobile.model.externalDevices.devices.Modbus
import com.example.setpointmanagermobile.model.registerCollection.mappers.DiscreteOutCollectionMapper

class DiscreteOutCollection () {
    var defence = WriteDefenceCollection()
    var timeSetIsUsed = true
    var timeUnsetIsUsed = true
    var weightIsUsed = true

    val items = mutableListOf<DiscreteOut>()

    constructor( mapper : DiscreteOutCollectionMapper) : this()
    {
        defence = WriteDefenceCollection( mapper.defenceCollection )
        for ( item in mapper.items )
        {
            items.add( DiscreteOut( item ) )
        }
        if ( items.isNotEmpty() )
        {
            timeSetIsUsed = items[ 0 ].timeSet.isUsed
            timeUnsetIsUsed = items[ 0 ].timeUnset.isUsed
            weightIsUsed = items[ 0 ].weight.isUsed
        }
    }

    constructor( baseCollection : DiscreteOutCollection) : this()
    {
        defence = WriteDefenceCollection( baseCollection.defence )
        timeSetIsUsed = baseCollection.timeSetIsUsed
        timeUnsetIsUsed = baseCollection.timeUnsetIsUsed
        weightIsUsed = baseCollection.weightIsUsed

        items.addAll( baseCollection.items )
    }

    fun checkRegistersUsage()
    {
        if ( !timeSetIsUsed )
        {
            items.forEach { it.timeSet.modscanCell = null }
        }
        if ( !timeUnsetIsUsed )
        {
            items.forEach { it.timeUnset.modscanCell = null }
        }
        if ( !weightIsUsed )
        {
            items.forEach { it.weight.modscanCell = null }
        }
    }

    fun checkCollectionFilling() : Boolean
    {
        println( "Collection check:" )
        var result = true
        val registers = mutableListOf<Int>()
        items.forEach { registers.add( it.values.registerNumber ) }
        if ( registers.size > registers.distinct().size )
        {
            result = false
            println( "Check sample registers for correct filling" )
        }

        registers.clear()
        items.forEach{ registers.add( it.setpoint.registerNumber ) }
        if ( registers.size > registers.distinct().size )
        {
            result = false
            println( "Check setpoint registers for correct filling" )
        }

        registers.clear()
        items.forEach { registers.add( it.setpointSample.registerNumber ) }
        if ( registers.size > registers.distinct().size )
        {
            result = false
            println( "Check registers of binding between setpoints and samples for correct filling" )
        }

        registers.clear()
        items.forEach { registers.add( it.timeSet.registerNumber ) }
        if ( timeSetIsUsed && ( registers.size > registers.distinct().size ) )
        {
            result = false
            println( "Check time setting registers for correct filling" )
        }

        registers.clear()
        items.forEach { registers.add( it.timeUnset.registerNumber ) }
        if ( timeUnsetIsUsed && ( registers.size > registers.distinct().size ) )
        {
            result = false
            println( "Check time unsetting registers for correct filling" )
        }

        registers.clear()
        items.forEach { registers.add( it.weight.registerNumber ) }
        if ( weightIsUsed && ( registers.size > registers.distinct().size ) )
        {
            result = false
            println( "Check weight registers for correct filling" )
        }
        
        return result
    }

    fun getParameterValues( device : Modbus) : Boolean
    {
        if ( items.isEmpty() )
        {
            return false
        }
        for ( i in items.indices )
        {
            if ( !items[i].getDiscreteOutValues( device ) )
            {
                return false
            }
        }
        return true
    }

    fun getSamples( n: Int, device : Modbus) : Boolean
    {
        if ( items.isEmpty() )
        {
            return false
        }
        items.forEach{ it.sample.clear() }
        for ( i in 0 until n )
        {
            items.filter { it.isUsed }.forEach{
                val ( success, value ) = it.getSampleValue( device )
                if ( ! success )
                {
                    return false
                }
                it.sample.add( value )
            }
        }
        return true
    }

    fun countSetpointValues( type : String, percent : Double )
    {
        for ( out in items.filter { it.isUsed } ) {
            out.setpoint.value = CountSetpoints.count(
                    CountSetpointsDescriptions.selectCountType(type),
                    out.sample, percent )
        }
    }

    fun writeParameterValues ( device : Modbus) : Boolean
    {
        if ( !unlockWrite( device ) )
        {
            return false
        }
        for ( i in items.indices )
        {
            if ( !items[ i ].writeDiscreteOutValues( device ) )
            {
                return false
            }
        }
        if ( !lockWrite( device ) )
        {
            return false
        }
        return true
    }

    fun unlockWrite( device : Modbus) : Boolean
    {
        return defence.open( device )
    }

    fun confirmWrite( device : Modbus) : Boolean
    {
        return defence.confirm( device )
    }

    fun lockWrite( device : Modbus) : Boolean
    {
        return defence.close( device )
    }
}