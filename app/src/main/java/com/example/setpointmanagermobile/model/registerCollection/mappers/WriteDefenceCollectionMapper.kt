package com.example.setpointmanagermobile.model.registerCollection.mappers

import com.example.setpointmanagermobile.model.registerCollection.WriteDefenceCollection

class WriteDefenceCollectionMapper() {
    val open = mutableListOf<RegisterMapper>()
    val confirm = mutableListOf<RegisterMapper>()
    val close = mutableListOf<RegisterMapper>()

    constructor( baseCollection : WriteDefenceCollection) : this()
    {
        baseCollection.openingRegisters.forEach { open.add( RegisterMapper( it ) ) }
        baseCollection.confirmingRegisters.forEach { confirm.add( RegisterMapper( it ) ) }
        baseCollection.closingRegisters.forEach { close.add( RegisterMapper( it ) ) }
    }
}