package com.example.setpointmanagermobile.model.registerCollection

import com.example.setpointmanagermobile.model.externalDevices.devices.Modbus
import com.example.setpointmanagermobile.model.registerCollection.mappers.WriteDefenceCollectionMapper

class WriteDefenceCollection() {
    val openingRegisters = mutableListOf<DiscreteOutRegister>()
    val confirmingRegisters = mutableListOf<DiscreteOutRegister>()
    val closingRegisters = mutableListOf<DiscreteOutRegister>()

    constructor( base : WriteDefenceCollection) : this()
    {
        openingRegisters.addAll( base.openingRegisters )
        confirmingRegisters.addAll( base.confirmingRegisters )
        closingRegisters.addAll( base.closingRegisters )
    }

    constructor( mapper : WriteDefenceCollectionMapper) : this()
    {
        if ( mapper.open != null ) {
            for (item in mapper.open) {
                openingRegisters.add( DiscreteOutRegister( item ) )
            }
        }
        if ( mapper.confirm != null ) {
            for (item in mapper.confirm) {
                confirmingRegisters.add( DiscreteOutRegister( item ) )
            }
        }
        if ( mapper.close != null )
        {
            for ( item in mapper.close )
            {
                closingRegisters.add( DiscreteOutRegister( item ) )
            }
        }
    }
    
    fun clearCollection()
    {
        openingRegisters.clear()
        confirmingRegisters.clear()
        closingRegisters.clear()
    }

    fun open( device : Modbus) : Boolean
    {
        var result = true
        openingRegisters.forEach { result = result && it.writeValue( it.value, device ) }
        return result
    }

    fun confirm( device : Modbus) : Boolean
    {
        var result = true
        confirmingRegisters.forEach { result = result && it.writeValue( it.value, device ) }
        return result
    }

    fun close( device : Modbus) : Boolean
    {
        var result = true
        closingRegisters.forEach { result = result && it.writeValue( it.value, device ) }
        return result
    }
}