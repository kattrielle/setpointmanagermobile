package com.example.setpointmanagermobile.model.registerMapTikModscan

enum class ElementType {
    Coil,
    DiscreteInput,
    HoldingRegister,
    InputRegister
}