package com.example.setpointmanagermobile.model.registerCollection.mappers

import com.example.setpointmanagermobile.model.registerCollection.DiscreteOut
import com.example.setpointmanagermobile.model.registerMapTikModscan.CellData

class DiscreteOutMapper() {
    var values : CellData? = null //регистр выборки
    var setpointSample : CellData? = null //регистр хранения адреса (номера регистра) выборки для уставки
    var setpoint : CellData? = null //регистр величины уставки
    var timeSet : CellData? = null //регистр времени устаноки для уставки
    var timeUnset : CellData? = null //регистр времени снятия для уставки
    var weight : CellData? = null //регистр весового коэффициента для уставки

    constructor( baseOut : DiscreteOut) : this() {
        values = baseOut.values.modscanCell
        setpointSample = baseOut.setpointSample.modscanCell
        setpoint = baseOut.setpoint.modscanCell
        timeSet = baseOut.timeSet.modscanCell
        timeUnset = baseOut.timeUnset.modscanCell
        weight = baseOut.weight.modscanCell
    }
}