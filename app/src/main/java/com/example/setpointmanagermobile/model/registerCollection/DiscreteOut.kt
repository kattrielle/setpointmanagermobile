package com.example.setpointmanagermobile.model.registerCollection

import com.example.setpointmanagermobile.model.externalDevices.devices.Modbus
import com.example.setpointmanagermobile.model.registerCollection.mappers.DiscreteOutMapper
import com.example.setpointmanagermobile.model.registerMapTikModscan.ElementFormat
import com.example.setpointmanagermobile.model.registerMapTikModscan.ElementType

class DiscreteOut () {
    var values = DiscreteOutRegister() //регистр выборки
    var setpointSample = DiscreteOutRegister() //регистр хранения адреса (номера регистра) выборки для уставки
    var setpoint = DiscreteOutRegister() //регистр величины уставки
    var timeSet = DiscreteOutRegister() //регистр времени устаноки для уставки
    var timeUnset = DiscreteOutRegister() //регистр времени снятия для уставки
    var weight = DiscreteOutRegister() //регистр весового коэффициента для уставки
    val sample : MutableList<Double> = mutableListOf()

    init {
        values.modscanCell?.initParams( ElementType.InputRegister, ElementFormat.Int )
        setpointSample.modscanCell?.initParams( ElementType.HoldingRegister, ElementFormat.Int )
        setpoint.modscanCell?.initParams( ElementType.HoldingRegister, ElementFormat.Int )
        timeSet.modscanCell?.initParams( ElementType.HoldingRegister, ElementFormat.Int )
        timeUnset.modscanCell?.initParams( ElementType.HoldingRegister, ElementFormat.Int )
        weight.modscanCell?.initParams( ElementType.HoldingRegister, ElementFormat.Int )
    }

    constructor( baseDiscreteOut : DiscreteOut) : this() {
        values.modscanCell = baseDiscreteOut.values.modscanCell
        setpointSample.modscanCell = baseDiscreteOut.setpointSample.modscanCell
        setpoint.modscanCell = baseDiscreteOut.setpoint.modscanCell
        timeSet.modscanCell = baseDiscreteOut.timeSet.modscanCell
        timeUnset.modscanCell = baseDiscreteOut.timeUnset.modscanCell
        weight.modscanCell = baseDiscreteOut.weight.modscanCell
    }

    constructor ( mapper : DiscreteOutMapper) : this() {
        values.modscanCell = mapper.values
        setpointSample.modscanCell = mapper.setpointSample
        setpoint.modscanCell = mapper.setpoint
        timeSet.modscanCell = mapper.timeSet
        timeUnset.modscanCell = mapper.timeUnset
        weight.modscanCell = mapper.weight
    }

    var isUsed = true

    fun getSampleValue( device: Modbus) : Pair< Boolean, Double >
    {
        return values.readConvertedValue( device )
    }

    fun getSample( n : Int, device : Modbus) : List<Double>
    {
        val result = mutableListOf<Double>()

        for ( i in 0 until n )
        {
            val ( success, sampleVal ) = getSampleValue ( device )
            if ( success )
            {
                result.add( sampleVal )
            }
        }
        return result
    }

    fun getDiscreteOutValues( device : Modbus) : Boolean
    {
        if ( !device.OpenConnection() )
        {
            return false
        }
        if ( !setpointSample.readValue( device ) )
        {
            return false
        }
        if ( setpointSample.value != 0.0 ) {
            isUsed = true
            if ( !setpoint.readValue( device ) )
            {
                return false
            }
            if ( timeSet.isUsed && !timeSet.readValue( device ) )
            {
                return false
            }
            if ( timeUnset.isUsed && !timeUnset.readValue( device ) )
            {
                return false
            }
            if ( weight.isUsed && !weight.readValue( device ) )
            {
                return false
            }
        } else {
            isUsed = false
        }
        return true
    }

    fun writeDiscreteOutValues( device : Modbus) : Boolean
    {
        var result = true
        result = result && setpointSample.writeValue( findSetpointSampleValue(), device )
        result = result && setpoint.writeValue( setpoint.value, device )
        if ( timeSet.isUsed ) {
            result = result && timeSet.writeValue( timeSet.value, device )
        }
        if ( timeUnset.isUsed ) {
            result = result && timeUnset.writeValue( timeUnset.value, device )
        }
        if ( weight.isUsed ) {
            result = result && weight.writeValue( weight.value, device )
        }
        return result
    }

    private fun findSetpointSampleValue() : Double
    {
        return if ( isUsed )
        {
            values.registerNumber.toDouble()
        } else {
            0.0
        }
    }

}